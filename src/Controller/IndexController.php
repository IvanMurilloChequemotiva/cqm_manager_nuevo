<?php

namespace CQM\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller {

    public function indexAction(Request $request){
        $options = array();

        return $this->render("Default/index.html.twig", $options);
    }

    public function getLoginAction(Request $request){
        $options = array();

        return $this->render("Default/login.html.twig", $options);
    }

    public function loginAction(Request $request){
        $options = array();

        var_dump($request->request->all());

        return $this->redirect("login", 302);
    }
}